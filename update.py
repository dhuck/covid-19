import pandas as pd
import altair as alt
import numpy as np
import requests
from iso3166 import countries
from vega_datasets import data
import re
import us
import datetime as dt
from sigfig import round
import selenium
import json
from altair_saver import save
import os
alt.data_transformers.disable_max_rows()

PWD = os.popen('pwd').read().rstrip()
VEGA_COUNTRIES = data.world_110m.url
VEGA_STATES = data.us_10m.url

def get_country_code(country):
    try:
        country_name = re.sub(r'[^a-zA-Z ]', '', country)
        country_code = str(countries.get(country_name).numeric)
    except KeyError:
        if country_name == 'Brunei':
            country_code = '096'
        elif country_name == 'Iran':
            country_code = '364'
        elif country_name == 'Korea South':
            country_code = '410'
        elif country_name == 'Vietnam':
            country_code = '704'
        elif country_name == 'Russia':
            country_code = '643'
        elif country_name == 'United Kingdom':
            country_code = '826'
        else:
            country_code = '-1'
    # strip leading zero on country codes for library
    return str(int(country_code))

def arrange_data(data, name, selector):
    if selector == 'Country':
        drop = 'Province/State'
        group = 'Country/Region'
    elif selector == 'State':
        drop = 'Country/Region'
        group = 'Province/State'

    data = data.drop(["Lat", "Long", drop], axis=1)
    data = data.groupby(data[group]).aggregate('sum').reset_index()
    data = data.melt(id_vars=group, var_name="Date", value_name=name)
    data[name] = data[name].astype('int32')
    data["Date"] = pd.to_datetime(data["Date"])
    return data.sort_values([group, "Date"]).reset_index().drop("index", axis=1)

def count_days(data, selector):
    if selector == 'Country':
        group = 'Country/Region'
    elif selector == 'State':
        group = 'Province/State'

    days = list(range(data["Date"].nunique())) * data[group].nunique()
    data["Days"] = days
    return data

def calc_total(data, selector):
    if selector == 'Country':
        group = 'Country/Region'
    elif selector == 'State':
        group = 'Province/State'

    dates = data["Date"].unique()
    cases, deaths, recoveries = [], [], []
    for date in dates:
        is_date = data["Date"] == date
        cases.append(data[is_date]["Cases"].sum())
        deaths.append(data[is_date]["Deaths"].sum())
        recoveries.append(data[is_date]["Recoveries"].sum())
    world = ["Worldwide"] * len(dates)
    total = pd.DataFrame({group: world,
                          "Date": dates,
                          "Cases": cases,
                          "Deaths": deaths,
                          "Recoveries": recoveries})
    data = data.append(total)
    return data.reset_index().drop("index", axis=1)

def get_id(data, selector):
    if selector == 'Country':
        ids = [get_country_code(row["Country"]) for index, row in data.iterrows()]
        data["id"] = ids
    elif selector == 'State':
        ids = [str(int(us.states.lookup(row["State"]).fips)) for index, row in data.iterrows()]
        data["id"] = ids
    return data


def clean_data(conf, dead, recv, selector):
    if selector == 'State':
        states = [state.name for state in us.STATES_AND_TERRITORIES]
        is_state = conf["Province/State"].isin(states)
        conf = conf[is_state]
        dead = dead[is_state]
        recv = recv[is_state]


    conf = arrange_data(conf, "Cases", selector)
    dead = arrange_data(dead, "Deaths", selector)
    recv = arrange_data(recv, "Recoveries", selector)

    conf["Deaths"] = dead["Deaths"]
    conf["Recoveries"] = recv["Recoveries"]

    if selector == 'Country':
        conf = conf.rename(columns={"Country/Region": "Country"})
    elif selector == 'State':
        conf = conf.rename(columns={"Province/State": "State"})
    conf = get_id(conf, selector)

    return conf

def get_scale(data, top, selector):
    top = round(float(top), 1)
    if selector == "Country":
        top = np.log10(top)
        scale_domain = list(np.logspace(1,top,num=12))
        scale_domain = [round(int(x), 1) for x in scale_domain]
    elif selector == "State":
        top = np.log10(top)
        scale_domain = list(np.logspace(1,top,num=12))
        scale_domain = [round(int(x), 1) for x in scale_domain]
    elif selector == "County":
        top = np.log10(top)
        scale_domain = list(np.logspace(0,top,num=12))
        scale_domain = [round(int(x), 1) for x in scale_domain]
        
    return scale_domain

def make_chart(data, title="", threshold=10, selector='Country', width=800):
    
    is_last = data["Date"] == data["Date"].unique()[-1]
    sorted_countries = data[is_last].sort_values(
        'Cases', ascending=False)[selector].to_list()
    current_cases = data[is_last]
    
    scale_domain = get_scale(current_cases, data["Cases"].max(), selector)
    
    # only show countries with more than a certain amount of cases in selection
    i = 0
    for country in sorted_countries:
        is_country = current_cases[selector] == country
        test = current_cases[is_country]["Cases"] > threshold
        if test.bool():
            i += 1
    
    on_click = alt.selection_single(fields=[selector], on='click')
    hover = alt.selection_single(fields=[selector], on='mouseover')
    
    
    # make the stats graph
    cases_chart=alt.Chart(data).transform_filter(
        on_click
    ).mark_bar(color='grey',binSpacing=0.5).encode(
        x=alt.X('monthdate(Date):T', title='Date'),
        y=alt.Y('sum(Cases):Q', title='Confirmed'),
    ).properties(
            width=width,
            height=200
    )
    
    deaths_chart=alt.Chart(data).transform_filter(
        on_click
    ).mark_line(color='red', thickness=2).encode(
        x=alt.X('monthdate(Date):T', title='Date'),
        y=alt.Y('sum(Deaths):Q', title='Deaths'),
    ).properties(
            width=width,
            height=75
    )
    
    if selector == 'Country':
        vega_map = alt.topo_feature(VEGA_COUNTRIES, 'countries')
        projection_type = 'mercator'
    elif selector == 'State':
        vega_map = alt.topo_feature(VEGA_STATES, 'states')
        projection_type = 'albersUsa'
    elif selector == 'County':
        vega_map = alt.topo_feature(VEGA_STATES, 'counties')
    
    background = alt.Chart(vega_map).mark_geoshape(fill="lightgrey")
    
    selected = alt.Chart(vega_map).mark_geoshape(fill="lightgrey").transform_lookup(
        lookup='id',
        from_=alt.LookupData(current_cases, 'id', [selector])
    ).encode(
        opacity=alt.condition(hover | on_click, alt.value(0), alt.value(0.5))
    )

    heatmap = alt.Chart(vega_map).mark_geoshape(stroke='white', strokeWidth=0.5).transform_lookup(
        lookup='id',
        from_=alt.LookupData(current_cases, 'id', ['Cases', selector, 'Deaths', 'Recoveries'])
    ).encode(
        color = alt.condition(
            'datum.Cases >= 1',
            alt.Color('Cases:Q', scale=alt.Scale(scheme='yelloworangered', 
                                                 type='threshold',
                                                 domain=scale_domain
                                                ), sort='ascending',
                                                   legend=alt.Legend(
                                                       type='symbol',
                                                       tickCount=10,
                                                       values=scale_domain,
                                                       format=',d',
                                                       labelFontSize=12,
                                                       symbolSize=200,
                                                       columns=3
                                                   )
                     ),
            alt.value('lightgrey')),
        tooltip = [alt.Tooltip(selector + ':N'),
                   alt.Tooltip('Cases:Q', format=',d'),
                   alt.Tooltip('Recoveries:Q', format=',d'),
                   alt.Tooltip('Deaths:Q', format=',d')]
    ).add_selection(
            hover,
            on_click
        )
    
    
    if selector == 'Country':
        map_projection = (background + heatmap + selected).properties(
            width=width,
            height=400
        ).project(
            type=projection_type,
            scale=100,
            center=[0,0],
            translate=[0,0],
            rotate=[0,0,0]
        )
    elif selector == 'State':
        map_projection = (background + heatmap + selected).properties(
            width=width,
            height=400
        ).project(
            type=projection_type,
#             scale=511,
#             center=[40,0]
        )
    final = (
        map_projection &
        cases_chart & deaths_chart
    ).properties(
        title=title,
    ).configure_title(
        fontSize=20,
        anchor='middle'
    ).configure_legend(
        orient='top'
    ).configure_view(
        strokeWidth=0
    )
        
    return final

def get_counties_daily():
    try:
        current = dt.datetime.today()
        current = current.strftime('%m-%d-%Y')
        url = f'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/{current}.csv'
        daily = pd.read_csv(url)
    except:
        current = dt.datetime.today() - dt.timedelta(days=1)
        current = current.strftime('%m-%d-%Y')
        url = f'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/{current}.csv'
        daily = pd.read_csv(url)
    
    is_us = daily["Country_Region"] == 'US'
    data = daily[is_us]
    data = data.dropna()
    data = data.rename({"FIPS": "id", "Admin2": "County"}, axis=1)
    data["id"] = data["id"].astype('int')
    data["id"] = data["id"].astype('str')
    data = data.drop(["Country_Region", "Lat", "Long_", "Active", "Last_Update", "Province_State"], axis=1)
    return data

def split_NYC(data):
    is_Kings = data["Combined_Key"] == "Kings, New York, US"
    kings_idx = int(data[is_Kings].index.to_list()[0])
    
    is_Queens = data["Combined_Key"] == "Queens, New York, US"
    queens_idx = int(data[is_Queens].index.to_list()[0])
    
    is_Richmond = data["Combined_Key"] == "Richmond, New York, US"
    richmond_idx = int(data[is_Richmond].index.to_list()[0])
    
    is_NYC = data["Combined_Key"] == "New York City, New York, US"
    nyc_idx = int(data[is_NYC].index.to_list()[0])
    
    confirmed = data.at[nyc_idx, "Confirmed"] // 3
    deaths = data.at[nyc_idx, "Deaths"] // 3
    recovered = data.at[nyc_idx, "Recovered"] // 3
    
    for idx in [kings_idx, queens_idx, richmond_idx]:
        data.at[idx, "Confirmed"] = confirmed
        data.at[idx, "Deaths"] = deaths
        data.at[idx, "Recovered"] = recovered
        
    data = data.drop(nyc_idx)
    
    return data

def county_chart(data, width=800):
    vega_map = alt.topo_feature(VEGA_STATES, 'counties')
    states = alt.topo_feature(VEGA_STATES, 'states')
    
    scale_domain = get_scale(data, data["Confirmed"].max(), "County")
    
    hover = alt.selection_single(fields=["Combined_Key"], on='mouseover')
    
    states_map = alt.Chart(states).mark_geoshape(stroke='white', strokeWidth=0.7, fillOpacity=0).project(
        type='albersUsa'
    )
    
    selected = alt.Chart(vega_map).mark_geoshape(fill="lightgrey").transform_lookup(
        lookup='id',
        from_=alt.LookupData(data, 'id', ["Combined_Key"])
    ).encode(
        opacity=alt.condition(hover, alt.value(0), alt.value(0.5))
    ).project(
        type='albersUsa'
    )

    base = alt.Chart(states).mark_geoshape(fill="lightgrey").project(
        type='albersUsa'
    )
    chart = alt.Chart(vega_map).mark_geoshape(strokeWidth=0.3, stroke='white').encode(
            color = alt.condition(
            'datum.Confirmed > 0',
            alt.Color('Confirmed:Q', scale=alt.Scale(scheme='yelloworangered', 
                                                 type='threshold',
                                                 domain=scale_domain
                                                ), sort='ascending',
                                                   legend=alt.Legend(
                                                       type='symbol',
                                                       tickCount=10,
                                                       values=scale_domain,
                                                       format=',d',
                                                       labelFontSize=12,
                                                       symbolSize=200,
                                                       columns=5
                                                   )
                     ),
                alt.value('lightgrey')
            ),
            tooltip = [alt.Tooltip('Combined_Key' + ':N', title="County"),
                      alt.Tooltip('Confirmed:Q', format=',d'),
                      alt.Tooltip('Recovered:Q', format=',d'),
                      alt.Tooltip('Deaths:Q', format=',d')]
    ).transform_lookup(
        lookup='id',
        from_=alt.LookupData(data, 'id', ['Confirmed', 'County', 'Deaths', 'Recovered', 'Combined_Key'])
    ).project(
        type='albersUsa'
    ).add_selection(
        hover
    )
    
    return (base + chart + states_map + selected).configure_legend(
            orient='top'
        ).configure_view(
            strokeWidth=0
        ).properties(
            width=width,
            height=500
        )

def main():
    new_country_signals = [
    { "name": "tx", "update": "width / 2" },
    { "name": "ty", "update": "height / 2" },
    {
      "name": "scale",
      "value": 100,
      "on": [{
        "events": {"type": "wheel", "consume": True},
        "update": "clamp(scale * pow(1.00625, -event.deltaY * pow(16, event.deltaMode)), 90, 750)"
      }]
    },
    {
      "name": "angles",
      "value": [0, 0],
      "on": [{
        "events": "mousedown",
        "update": "[rotateX, centerY]"
      }]
    },
    {
      "name": "cloned",
      "value": None,
      "on": [{
        "events": "mousedown",
        "update": "copy('projection')"
      }]
    },
    {
      "name": "start",
      "value": None,
      "on": [{
        "events": "mousedown",
        "update": "invert(cloned, xy())"
      }]
    },
    {
      "name": "drag", "value": None,
      "on": [{
        "events": "[mousedown, window:mouseup] > window:mousemove",
        "update": "invert(cloned, xy())"
      }]
    },
    {
      "name": "delta", "value": None,
      "on": [{
        "events": {"signal": "drag"},
        "update": "[drag[0] - start[0], start[1] - drag[1]]"
      }]
    },
    {
      "name": "rotateX", "value": 0,
      "on": [{
        "events": {"signal": "delta"},
        "update": "angles[0] + delta[0]"
      }]
    },
    {
      "name": "centerY", "value": 80,
      "on": [{
        "events": {"signal": "delta"},
        "update": "clamp(angles[1] + delta[1], -20, 80)"
      }]
    }
  ]

    new_country_projection = [
          {
            "name": "projection",
            "type": "mercator",
            "scale": {"signal": "scale"},
            "rotate": [{"signal": "rotateX"}, 0, 0],
            "center": [0, {"signal": "centerY"}],
            "translate": [{"signal": "tx"}, {"signal": "ty"}]
          }
      ]
    
    print("Getting data from Github...")
    confirmed = pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv")
    deaths    = pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv")
    recovered = pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv")

    confirmed_arch    = pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/archived_data/archived_time_series/time_series_19-covid-Confirmed_archived_0325.csv").drop(['3/23/20'], axis=1)
    deaths_arch       = pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/archived_data/archived_time_series/time_series_19-covid-Deaths_archived_0325.csv")
    recovered_arch    = pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/archived_data/archived_time_series/time_series_19-covid-Recovered_archived_0325.csv")

    print("Cleaning data...")
    country_dataset = clean_data(confirmed, deaths, recovered, "Country")
    state_dataset   = clean_data(confirmed_arch, deaths_arch, recovered_arch, "State")
    united_states   = get_counties_daily()
    
    # split NYC
    united_states   = split_NYC(united_states)

    print("Building graphs....")
    world = make_chart(country_dataset, threshold=10, width='container')
    states = make_chart(state_dataset, threshold=1, selector="State")
    counties = county_chart(united_states, width='container')


    print("Saving graphs...")
#     world.save(f'{PWD}/data/world_covid.json', embed_options={"actions": False})
    states.save(f'{PWD}/data/state_covid.json', embed_options={"actions": False})
    counties.save(f'{PWD}/data/counties_covid.json', embed_options={"actions": False})

    state_file    = f'{PWD}/data/state_covid.vg.json'
    counties_file = f'{PWD}/data/counties_covid.vg.json'
    world_file    = f'{PWD}/data/world_covid.vg.json'
    
    save(counties, counties_file)
    save(states, state_file)
    save(world, world_file)
    
    # inject special json into world file
    with open(world_file, 'r') as f:
        country_json = json.load(f)
        country_json['signals'] = country_json['signals'] + new_country_signals
        country_json['projections'] = new_country_projection
        out_file = open(f'{PWD}/data/world_covid.json', 'w')
        json.dump(country_json, out_file)

if __name__ == '__main__':
    main()
else:
    pass

